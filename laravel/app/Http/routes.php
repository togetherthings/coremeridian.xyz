<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'mobile'], function () {
    Route::post('register', 'Auth\DeviceAuthController@register');
    Route::post('login', 'Auth\DeviceAuthController@login');
    Route::get('logout', 'Auth\DeviceAuthController@logout');
});

Route::group(['middleware' => 'auth:api', 'prefix' => 'restricted'], function () {
    Route::get('users', 'TestController@how_many_users');
});

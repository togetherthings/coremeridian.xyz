<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;

class DeviceAuthController extends AuthController
{
    public function register(Request $request)
    {
	if ($this->device_request_validated($request)) {
	    $user = $this->create($request->all());

	    if ($user->id) {
		$credentials = $request->only('device_id', 'password');
                $response = parent::authenticate($credentials);
		return response()->json($response);
	    }
	    else {
		return response()->json(['error', 'could_not_create_user']);
	    }
	}
	else {
	    return response()->json(['error', 'request_denied']);
	}
    }

    public function login(Request $request)
    {
        if ($this->device_request_validated($request)) {
	    $user = parent::get_requested_user($request);
	    if (parent::user_has_active_status($user)) {
		$credentials = $request->only('device_id', 'password');
		$response = parent::authenticate($credentials);
                return response()->json($response);
	    }
	    else {
		return response()->json(['error', 'user_status_not_active']);
	    }
	}
	else {
	    return response()->json(['error', 'request_denied']);
	}
    }

    public function logout() {
	Auth::guard($this->getGuard())->logout();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'device_id' => 'required|min:10',
	    'password' => 'required|min:10',
	    'location' => 'required',
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
	    'device_id' => $data['device_id'],
	    'password' => bcrypt($data['password']),
	    'status' => 'active',
	    'location' => $data['location'],
	]);
    }

    protected function device_request_validated(Request $request)
    {
	$validator = $this->validator($request->all());

        if ($validator->fails()) {
	    return false;
        }

	return true;
    }
}

<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Http\Request;

class TestController extends Controller
{
    protected function how_many_users(Request $request)
    {
	return response()->json(['user_count', User::all()->count()]);
    }
}
